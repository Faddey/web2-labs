'use strict';


angular.module("ToDoListApp", [
]).controller("ToDoListCtrl", function ($scope) {

    $scope.reset = function () {
        $scope.data = {
            name: {
                value: '',
                valid: false,
                touched: false
            },
            email: {
                value: '',
                valid: false,
                touched: false
            },
            phone: {
                value: '',
                valid: false,
                touched: false
            },
            password: {
                value: '',
                valid: false,
                touched: false
            },
            passwordConfirm: {
                value: '',
                valid: false,
                touched: false
            }
        };
        $scope.submitted = false;
        $scope.tried_submit = false;
    };
    $scope.reset();

    $scope.validate = function (field) {
        console.log($scope.data);
        $scope.data[field].touched = true;
        if ($scope.data.name.touched) {
            $scope.data.name.valid = $scope.data.name.value.length > 5;
        }
        if ($scope.data.email.touched) {
            var email = $scope.data.email.value.trim();
            var idx = email.indexOf('@');
            // '@' character should be somewhere in the middle
            $scope.data.email.valid = idx > 0 && idx < email.length-1;
        }
        if ($scope.data.phone.touched) {
            var phone = $scope.data.phone.value.replace(/\s/g, '');
            var matches = phone.match(/\+\d{10}/g);
            if (!!matches) {
                $scope.data.phone.valid = (matches[0] == phone);
            } else {
                $scope.data.phone.valid = false;
            }
        }
        if ($scope.data.password.touched) {
            $scope.data.password.valid = $scope.data.password.value.length > 5;

            if ($scope.data.password.valid && $scope.data.passwordConfirm.touched) {
                $scope.data.passwordConfirm.valid =
                    $scope.data.password.value == $scope.data.passwordConfirm.value;
            }
        }
    };
    $scope.isValid = function () {
        return Object.keys($scope.data)
            .map(function(fld) { return $scope.data[fld].valid; })
            .reduce(function(acc, val) { return acc && val; }, true);
    };
    $scope.submit = function () {
        $scope.tried_submit = true;
        if ($scope.isValid()) {
            $scope.submitted = true;
        }
    }

});
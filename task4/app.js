'use strict';


var DATA = {
    questions: [
        {
            text: 'asdasdasd',
            variants: [
                {text: '111', correct: true},
                {text: '222', correct: false},
                {text: '333', correct: false},
                {text: '444', correct: false}
            ]
        },
        {
            text: 'zxczxczxc',
            variants: [
                {text: '111', correct: false},
                {text: '222', correct: true},
                {text: '333', correct: false},
                {text: '444', correct: false}
            ]
        },
        {
            text: 'qweqweqwe',
            variants: [
                {text: '111', correct: false},
                {text: '222', correct: false},
                {text: '333', correct: true},
                {text: '444', correct: false}
            ]
        },
        {
            text: 'ertertert',
            variants: [
                {text: '111', correct: false},
                {text: '222', correct: false},
                {text: '333', correct: false},
                {text: '444', correct: true}
            ]
        }
    ]
};


angular.module("ToDoListApp", [
]).controller("ToDoListCtrl", function ($scope) {

    $scope.score = null;

    $scope.questions = DATA.questions.map(function (question) {
        return {
            text: question.text,
            variants: question.variants,
            correct: false,
            answered: false
        }
    });
    $scope.currentQuestion = null;

    // Обработчик нажатия по кнопке
    $scope.goToQuestion = function (question) {
        $scope.currentQuestion = question;
    };

    $scope.answerQuestion = function (question, answer) {
        question.correct = answer.correct;
        question.answered = true;
        $scope.currentQuestion = null;
        console.log($scope.questions);
        if (nRemainingToAnswer() == 0) {
            $scope.submit();
        }
    };

    $scope.submit = function () {
        var score = 0;
        $scope.questions.forEach(function (q) {
            if (q.correct) score++;
        });
        $scope.score = score;
    };

    $scope.reset = function () {
        $scope.score = null;
        $scope.currentQuestion = null;
        $scope.questions.forEach(function (q) {
            q.correct = false;
            q.answered = false;
        })
    };

    $scope.getCurrentPage = function () {
        if ($scope.score !== null) return 'results';
        return ($scope.currentQuestion === null) ? 'list' : 'question';
    };

    function nRemainingToAnswer() {
        var n = 0;
        $scope.questions.forEach(function (q) {
            if (!q.answered) n++;
        });
        return n;
    }

});
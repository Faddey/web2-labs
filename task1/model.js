var model = {
    user: "awesome user",
    courses: [
        { name: "HTML, CSS", passed: true, mark: 76 },
        { name: "JavaScript Essential", passed: true, mark: 91 },
        { name: "JavaScript Advanced", passed: true, mark: 85 },
        { name: "HTML5; CSS3", passed: false },
        { name: "AngularJS", passed: false }
    ]
};
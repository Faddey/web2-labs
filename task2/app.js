'use strict';

angular.module("ToDoListApp", [
    'ngStorage'
]).controller("ToDoListCtrl", function ($scope, $localStorage) {

    $scope.tasks = $localStorage.$default({'task2': {
        tasks: []
    }})['task2'].tasks;

    $scope.addNewTask = function () {

        $scope.tasks.push({
            name: $scope.newTaskName,
            description: $scope.newTaskDescription,
            done: false
        });

        $scope.newTaskName = "";
        $scope.newTaskDescription = "";
    };
});